<!DOCTYPE html>
<?php require 'kredence.php'; require 'konekce.php';?>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <title><?=$title?></title>
  </head>
  <body>
    <div class="content-wrap" style="text-align:center;">
      <h1 class='title'><?php echo $title ?></h1>
      <h2 class='postdata'>Search for an Article</h2>
      <form method="post">
        <label for="search">Article ID</label>
        <input type="number" name="search" id="search" size=1>
        <input type="submit" name="submit" value="Search">
      </form>
    <!-- </div> -->
    <?php
    if(isset($_POST["submit"])) {
      function clean_text($string)
      {
         $string = trim($string);
         $string = stripslashes($string);
         $string = htmlspecialchars($string);
         $string = preg_replace("/[^0-9]/", "", $string);
         return $string;
      }
      $search = clean_text($_POST['search']);
      $sql = "SELECT * FROM $tablename WHERE id LIKE '$search'";
      $result = $conn->query($sql);

      if (!$result->num_rows > 0) {
        echo "<p>Sorry, no such item exists.</p></div>";
      } else {
        echo "</div><div class=\"content-wrap\">";
        $row = $result->fetch_assoc();
        $title = $row["nameArticles"];
        $datepub = $row["datePublic"];
        $author = $row["author"];
        $category = $row["category"];
        $img = $row["img"];
        $imgsrc = "<img src=$img>";
        $content = $row["contentArticles"];
        echo "<h1 class='title'>$title</h1>";
        echo "<div class='postdata'>Published $datepub by $author in $category</div>";
        echo "<img src=$img class=\"center\">";
        echo "<div class='content'>$content</div>";
        /*echo "<table class=\"w3-table-all\"><tr>
        <th>Item Code</th>
        <th>Item Description</th>
        <th>Item Stock Quantity</th>
        <th>Item Unit Price</th>
        <th>Item Category</th></tr>";
        while($row = $result->fetch_assoc()) {
          echo "<tr>
          <td>LOS".$row["ItemCode"]."</td>
          <td>".$row["ItemDesc"]."</td>
          <td>".$row["ItemStockQty"]."</td>
          <td>".$row["ItemUnitPrice"]."</td>
          <td>".$row["ItemCategory"]."</td>
          </tr>";
        }
        echo "</table>";*/
        echo "</div>";
      }
      $conn->close();
    }
    ?>
  </body>
</html>
